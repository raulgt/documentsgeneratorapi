﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentsGeneratorApi.ViewModels
{
    public class NewNumber
    {
        public string SerialValue { get; set; }

        public string SerialNumber { get; set; }

    }
}
