﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentsGeneratorApi.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NumberingSetup",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CustomerNos = table.Column<Guid>(nullable: false),
                    QuoteNos = table.Column<Guid>(nullable: false),
                    InvoiceNos = table.Column<Guid>(nullable: false),
                    CreditMemoNos = table.Column<Guid>(nullable: false),
                    DebitNos = table.Column<Guid>(nullable: false),
                    EmployeesNos = table.Column<Guid>(nullable: false),
                    InfoPrinterNos = table.Column<Guid>(nullable: false),
                    ItemJournalNos = table.Column<Guid>(nullable: false),
                    ReservationJournalNos = table.Column<Guid>(nullable: false),
                    Statement = table.Column<Guid>(nullable: true),
                    CambiodeTdc = table.Column<Guid>(nullable: true),
                    InvoiceDfacture = table.Column<Guid>(nullable: true),
                    CreditMemoDfacture = table.Column<Guid>(nullable: true),
                    DebitDfacture = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NumberingSetup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SequenceTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SequenceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SerialNumbersShelf",
                columns: table => new
                {
                    SequenceTypeId = table.Column<Guid>(nullable: false),
                    SerialValue = table.Column<int>(nullable: false),
                    IsHold = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SerialNumbersShelf", x => x.SequenceTypeId);
                });

            migrationBuilder.CreateTable(
                name: "SerialSchemas",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    InitialValue = table.Column<int>(nullable: true),
                    Increment = table.Column<int>(nullable: true),
                    SerialMask = table.Column<string>(nullable: true),
                    LeadingZerosLength = table.Column<int>(nullable: true),
                    ExpiryDate = table.Column<DateTime>(nullable: true),
                    MaxValue = table.Column<int>(nullable: true),
                    IsCycle = table.Column<int>(nullable: true),
                    LastSerial = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SerialSchemas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NumberingSetup");

            migrationBuilder.DropTable(
                name: "SequenceTypes");

            migrationBuilder.DropTable(
                name: "SerialNumbersShelf");

            migrationBuilder.DropTable(
                name: "SerialSchemas");
        }
    }
}
