﻿using EngineGeneratorApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentsGeneratorApi.Data
{
    public class GeneratorContext : DbContext
    {
        public GeneratorContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }


        public DbSet<NumberingSetup> NumberingSetup { get; set; }
        public DbSet<SequenceTypes> SequenceTypes { get; set; }
        public DbSet<SerialNumbersShelf> SerialNumbersShelf { get; set; }
        public DbSet<SerialSchemas> SerialSchemas { get; set; }
    }
}

