﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EngineGeneratorApi.Models
{
    public partial class SequenceTypes
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
