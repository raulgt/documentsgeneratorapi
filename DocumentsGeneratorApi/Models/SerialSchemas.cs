﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EngineGeneratorApi.Models
{
    public partial class SerialSchemas
    {
        [Key]
        public Guid Id { get; set; }
        public string Description { get; set; }
        public int? InitialValue { get; set; }
        public int? Increment { get; set; }
        public string SerialMask { get; set; }
        public int? LeadingZerosLength { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? MaxValue { get; set; }
        public int? IsCycle { get; set; }
        public int? LastSerial { get; set; }
    }
}
