﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EngineGeneratorApi.Models
{
    public partial class NumberingSetup
    {
        [Key]
        public string Id { get; set; }
        public Guid CustomerNos { get; set; }
        public Guid QuoteNos { get; set; }
        public Guid InvoiceNos { get; set; }
        public Guid CreditMemoNos { get; set; }
        public Guid DebitNos { get; set; }
        public Guid EmployeesNos { get; set; }
        public Guid InfoPrinterNos { get; set; }
        public Guid ItemJournalNos { get; set; }
        public Guid ReservationJournalNos { get; set; }
        public Guid? Statement { get; set; }
        public Guid? CambiodeTdc { get; set; }
        public Guid? InvoiceDfacture { get; set; }
        public Guid? CreditMemoDfacture { get; set; }
        public Guid? DebitDfacture { get; set; }
    }
}
