﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EngineGeneratorApi.Models
{
    public partial class SerialNumbersShelf
    {
        [Key]
        public Guid SequenceTypeId { get; set; }
        public int SerialValue { get; set; }
        public bool IsHold { get; set; }
    }
}
