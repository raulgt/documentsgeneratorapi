﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using DocumentsGeneratorApi.Data;
using DocumentsGeneratorApi.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

using System.Data;

namespace DocumentsGeneratorApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private readonly GeneratorContext _context;

        public ValuesController(GeneratorContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<String> Get()
        {
             return "Value";
        }

        // GET api/values/5
        [HttpGet("{store}/{columnName}")]
        public ActionResult<NewNumber> Get(string store,string columnName)
        {
            try
            {
                string code = string.Empty;   

                string value1, value2;
                NewNumber respond = new NewNumber();

                string cs = _context.Database.GetDbConnection().ConnectionString;

                using (SqlConnection con = new SqlConnection(cs))
                {

                   // Se abre la conexion
                    con.Open();

                    // Consultamos el primer query que posteriormente hay que modificar para no utilizar query dinamico
             
                    string query1 = string.Format("SELECT {0} FROM CodeGenerator.dbo.NumberingSetup WHERE Id = '{1}'", columnName, store);

                    SqlCommand cmd = new SqlCommand(query1, con);

                    SqlDataReader reader = cmd.ExecuteReader();

                  if (reader.Read())
                    {
                        code = String.Format("{0}", reader[columnName]);
                    }
                    con.Close();
                    

                    // A partir de aqui el segundo query
                    con.Open();

                    cmd.CommandText = ("uspGenerateSerialNumber");

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@SequenceType", code);

                    SqlParameter outputParameterFirts = new SqlParameter();
                    SqlParameter outputParameterSecond = new SqlParameter();

                    outputParameterFirts.ParameterName = "@SerialValue";
                    outputParameterSecond.ParameterName = "@SerialNumber";

                    outputParameterFirts.SqlDbType = SqlDbType.BigInt;
                    outputParameterSecond.SqlDbType = SqlDbType.NVarChar;
                    outputParameterSecond.Size = 100;

                    outputParameterFirts.Direction = ParameterDirection.Output;
                    outputParameterSecond.Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(outputParameterFirts);
                    cmd.Parameters.Add(outputParameterSecond);

                
                    cmd.ExecuteNonQuery();

                    value1 = Convert.ToString(outputParameterFirts.Value);
                    value2 = Convert.ToString(outputParameterSecond.Value);

                    con.Close();
                }

                respond.SerialValue = value1;
                respond.SerialNumber = value2;
                                

                return respond;
            }
            catch (Exception e)
            {

                return Content(e.Message +" / "+ e.InnerException );
            }         
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
